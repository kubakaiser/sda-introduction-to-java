package day7.abstraction;

public class Kwadrat extends Figura {
    private double a;

    public Kwadrat(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    @Override
    public double obliczPole() {
        System.out.println("pole kwadratu");
        return a * a;
    }

    @Override
    public double obliczObwod() {
        return 4 * a;
    }
}
