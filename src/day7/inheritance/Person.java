package day7.inheritance;

// Klasa bazowa - super class
public class Person {
    protected String name;
    private String surname;
    private int age;

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String describe() {

        return this.name + " " + this.getSurname() + " ";
    }

    @Override
    public String toString() {
        return this.name + " " + this.getSurname() + " ";
    }
}
