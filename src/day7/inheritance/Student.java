package day7.inheritance;

// klasa pochodna, rozszerzjaca - sub class
public class Student extends Person {

    private String index;
    private String university;

    public Student(String name, String surname, int age) {
        super(name, surname, age);
    }

    public Student(String name, String surname, int age, String index, String university) {
        super(name, surname, age);
        this.index = index;
        this.university = university;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    @Override
    public String describe() {
        String opis = super.describe();
        return String.format("%s i jestem studentem %s", opis, getUniversity());
//        return String.format("%s %s i jestem studentem %s", getName(), getSurname(), getUniversity());
    }

    @Override
    public String toString() {
        return String.format("%s %s i jestem studentem %s", getName(), getSurname(), getUniversity());
    }
}
