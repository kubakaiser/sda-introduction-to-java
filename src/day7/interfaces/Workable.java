package day7.interfaces;

public interface Workable {
    void work();

    double getSalary();
}
