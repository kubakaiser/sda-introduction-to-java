package companyManager;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "company")
public class Company {
    private static final int DEFAULT_SIZE = 50;
    private String name;
    private Employee[] employees;
    private int companySize = 0;

    private Company() {

    }

    public Company(String name) {
        this.name = name;
        this.employees = new Employee[DEFAULT_SIZE];
    }

    public Company(String name, int initialSize) {
        this.name = name;
        this.employees = new Employee[initialSize];
    }

    public boolean addEmployee(Employee emp) {
        if (companySize < employees.length) {
            employees[companySize++] = emp;
            return true;
        }
        return false;
    }

    public void addEmployees(Employee[] employees) {
        for (int i = 0; i < employees.length; i++) {
            this.addEmployee(employees[i]);
        }
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @XmlElement(name = "employee")
    public Employee[] getEmployees() {
        return employees;
    }
    public void setEmployees(Employee[] employees) {
        this.employees = employees;
    }
}
