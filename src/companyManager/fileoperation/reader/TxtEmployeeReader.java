package companyManager.fileoperation.reader;

import companyManager.Employee;
import companyManager.fileoperation.util.ParseUtil;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

//Konkretna klasa umozliwiajaca czytanie z pliku txt

public class TxtEmployeeReader extends AbstractEmployeeReader {
    public TxtEmployeeReader(String pathToFile) {
        super(pathToFile);
    }

    //Metoda do odczytu pracownikow (z intefejsu)
    @Override
    public Employee[] readEmployees() {
        Employee[] employees = null;
        int i = 0;
        // Try with resources
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToFile))) {
            String companySize = bufferedReader.readLine();
            employees = new Employee[Integer.parseInt(companySize)];
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                String[] split = line.split(" ");
                Employee emp = new Employee(split[1], split[2]);
                emp.setId(Integer.parseInt(split[0]));
                emp.setEmail(split[3]);
                emp.setSalary(ParseUtil.parseDouble(split[4]));
                emp.setAge(Integer.parseInt(split[5]));
                employees[i++] = emp;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return employees;
    }
}
