package day2.instrukcjesterujace;

import java.util.Scanner;

public class IfStatement {
    public static void main(String[] args) {
        int wiek;

        System.out.println("Podaj wiek");
        Scanner odczyt = new Scanner(System.in);
        wiek = odczyt.nextInt();


        if (wiek >= 18) {
            System.out.println("Jestes pelnoletni");
        } else {
            System.out.println("Nie jestes pelnoletni");
        }

        System.out.println("Witaj w systemie");

    }
}
