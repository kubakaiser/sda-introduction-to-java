package day2.instrukcjesterujace;

import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args) {

        System.out.println( 10 / 0);
        double a;
        double b;
        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj wartosci a i b");
        a = odczyt.nextDouble();
        System.out.println("Podaj b: ");
        b = odczyt.nextDouble();

        System.out.println("--------------------");
        System.out.println("Wybierz opcje: ");
        System.out.println("1. Dodawanie");
        System.out.println("2. Odejmowanie");
        System.out.println("3. Mnozenie");
        System.out.println("4. Dzielenie");
        int wybor = odczyt.nextInt();

        // etap 2 ............
        switch (wybor) {
            case 1:
                System.out.println("Wynik: " + (a + b));
                break;
            case 2:
                System.out.println(a - b);
                break;
            case 3:
                System.out.println(a * b);
                break;
            case 4:
                System.out.println(a / b);
                break;
            default:
                System.out.println("Zly wybor");
        }


    }
}
