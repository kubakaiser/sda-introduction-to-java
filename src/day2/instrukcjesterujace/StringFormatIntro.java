package day2.instrukcjesterujace;

public class StringFormatIntro {
    public static void main(String[] args) {

        String imie = "Kuba";
        String nazwisko = "Kaiser";
        double wzrost= 180.7465;

        String kom = String.format("Masz na imie %s a nazywasz sie %s i masz %10.2f lat", imie, nazwisko, wzrost);
        System.out.println(kom);
    }

}
