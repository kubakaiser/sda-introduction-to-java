package day2.odczytzkonsoli;

import java.util.Scanner;

/*
    Program pobierajacy imie, nazwisko i wiek
    i drukujacy te informacje na konsoli
 */
public class ScannerIntro {
    public static void main(String[] args) {
        // zapytaj uzytkownika o imie
        String imie;
        String nazwisko;
        int wiek;

        System.out.println("Jak masz na imie?");

        //utworzenie obiektu typu Scanner o nazwie odczyt
        Scanner odczyt = new Scanner(System.in);

        //do zmiennej imie wstaw nastepna linie z konsoli
        imie = odczyt.nextLine();

        //odczytaj z konsoli linie tekstu i wstaw do zmiennej nazwisko
        System.out.println("Podaj nazwisko: ");
        nazwisko = odczyt.nextLine();

        //odczytaj z konsoli liczbe i wstaw do zmiennej wiek
        System.out.println("Podaj wiek");
        wiek = odczyt.nextInt();


        System.out.println("Witaj, " + imie + " " + nazwisko+ " masz: " + wiek + " lat ;)");

    }
}
