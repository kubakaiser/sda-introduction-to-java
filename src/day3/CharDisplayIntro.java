package day3;

// Wyswietlic znaki od A do Z
// z uzyciem petli for
public class CharDisplayIntro {

    public static void wypiszParzyste() {
        for (int i = 0; i < 100; i += 2) {
            System.out.println(i);
        }
    }

    //funkcja ktora wypisuje napis przyjety jako argument
    public static void wypiszNapis(String name) {
        System.out.println("Argument to: " + name);
    }

    // funkcja dodajaca dwie liczby
    public static int dodajLiczby(int a, int b) {
        System.out.println("Dodaje dwie liczby...........");
        int wynik = a + b;
        return wynik;
    }

    public static void main(String[] args) {

    }
}
