package day3;

import java.util.Scanner;

/*
    Petla while drukujaca liczby od 0 100
 */
public class PetlaWhile {
    public static void main(String[] args) {

        // zapytac uzytkownika o zakres liczb
        System.out.println("Podaj zakres liczb");
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();

        //wypisac parzyste z tego zakresu
        // a = 10   b = 50
        while (a <= b) {
            if (a % 2 == 0) {
                System.out.println(a);
            }
            a++;
        }
        System.out.println("Koniec petli  ");
    }
}
