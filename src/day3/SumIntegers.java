package day3;

public class SumIntegers {
    public static void main(String[] args) {

        //inicjalizujemy pojemnik na sume
        int sum = 0;

        //z kazdym obiegiem petli zwiekszaj pojemnik o wartosc i
        for (int i = 1; i <= 100; i++) {
            sum += i;
        }

        System.out.println("Suma liczb to: " + sum);
    }
}
