package day3;

// od 0  -  100
// wypisac nieparzyste z zakresu 0 - 100 z uzyciem do while
public class PetlaDoWhile {
    public static void main(String[] args) {

        int licznik = 101;
        do {
            System.out.println("jestem w do while");
            if(licznik % 2 != 0){
                System.out.println(licznik);
            }
            licznik++;
        } while (licznik < 100);

        int wynik = 101;

        while (wynik < 100){
            System.out.println("jestem w while");
            System.out.println(wynik);
        }

    }
}
