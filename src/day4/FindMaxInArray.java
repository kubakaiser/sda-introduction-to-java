package day4;

import java.util.Random;

// Wylosuj 10 wartosci i znajdz najwieksza
public class FindMaxInArray {
    public static void main(String[] args) {
        Random rd = new Random();
        int[] array = new int[10];

        // wpisywanie losowych wartosci to tablicy
        for (int i = 0; i < array.length; i++) {
            array[i] = rd.nextInt(50);
        }

        // wypisz wartosci
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        // szukanie max
        // zakladamy wstepnie, ze najwieksza wartosc jest w tablicy o indeksie 0
        //jezeli spotkamy wieksza od aktualnego maximum
        //to podmieniamy
        int max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        System.out.println("\nNajwieksza wartosc to: " + max);
    }
}
