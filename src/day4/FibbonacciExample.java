package day4;

import java.util.Scanner;

public class FibbonacciExample {
    public static void main(String[] args) {
        // pobrac n z klawiatury
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj n");
        int n = sc.nextInt();
        // wywolac funkcje fibonacci
        // z Klasy MathHelper
        int wynik = MathHelper.fibonacci(n);

        //wydrukowac wynik
        System.out.println(n + " -ty wyraz ciagu to: " + wynik);
    }
}
