package day4;

import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy: ");
        int rozmiar = scanner.nextInt();
        int[] tablica = new int[rozmiar];

        //wpisywanie wartosci przez uzytkownika
        //tyle ile tablica ma elementow,
        // tyle razy prosimy o podanie liczby
        for (int i = 0; i < tablica.length; i++) {
            //do i-tego elementu tablicy wpisz wartosc od uzytkownika
            System.out.println("Podaj " + (i + 1) + "wartosc: ");
            tablica[i] = scanner.nextInt();
        }

        // wyswietlanie wartosci - wyswietl tylko,
        // jezeli wartosc jest z przedzialu 4 (wlacznie) do 15 (bez tej wartosci)

        for (int i = 0; i < tablica.length; i++) {
            //wypisz tylko jezeli jest w okreslonym przedziale
            if (tablica[i] >= 4 && tablica[i] < 15) {
//                System.out.print(tablica[i] + " ");
//                System.out.println("Indeks: " + i + ", wartosc: " + tablica[i]);
            }
        }
    }
}
