package day4;

import java.util.Scanner;

public class Silnia {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe");
        int liczba = sc.nextInt();

        // obliczanie silni
        int wynik = MathHelper.factorial(liczba);

        System.out.println("Wynik to: " + wynik);
    }
}
