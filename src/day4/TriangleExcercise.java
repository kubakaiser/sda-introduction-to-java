package day4;

import java.util.Scanner;

public class TriangleExcercise {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c = sc.nextDouble();

        if ((a + b > c) && (a + c > b) && (b + c > a)) {
            System.out.println("Mozna zrobic trojkat");
            double pole = MathHelper.obliczPole(a, b, c);

            System.out.println(String.format("Pole to: %.3f", pole));
        } else {
            System.out.println("Nie mozna zrobic trojkata");
        }
    }
}
