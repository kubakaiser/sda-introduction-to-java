package day4;

import java.util.Scanner;

/*
        Napisz pr wczytujący z klawiatury n liczb całkowitych. Liczbę n należy pobrać od
        użytkownika. Jeśli podana wartość jest z zakresu 1-30, wówczas należy pobrać podaną ilość
        liczb całkowitych, a następnie wydrukować każdą z liczb podniesioną do kwadratu na
        ekranie. Jeśli liczba jest spoza tego przedziału należy zakończyć pracę drukując stosowny
        komunikat.
        */
public class ArrayExample {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj wartosc z zakresu 1-30");
        int rozmiar = sc.nextInt();

        if (rozmiar > 0 && rozmiar <= 30) {
            int[] tab = new int[rozmiar];
            for (int i = 0; i < tab.length; i++) {
                tab[i] = i * i;
            }
            System.out.println("Wartosci tablicy ------");
            for (int i = 0; i < tab.length; i++) {
                System.out.println(tab[i]);
            }

        } else {
            System.out.println("Zly zakres - koniec pracy");
        } }
}
