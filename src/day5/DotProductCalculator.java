package day5;

import java.util.Random;

public class DotProductCalculator {

    // deklaracja - okresla rozmiar tablicy
    public static final int ARRAY_SIZE = 30;

    //okresla gorne ograniczenie generatora liczb pseudoloswych
    public static final int RANDOM_UPPER_BOUND = 50;

    public static void main(String[] args) {

        Random rd = new Random();
        int[] arr = new int[ARRAY_SIZE];
        int[] arr2 = new int[ARRAY_SIZE];

        // losowanie wartosci

        for (int i = 0; i < ARRAY_SIZE; i++) {
            arr[i] = rd.nextInt(RANDOM_UPPER_BOUND) + 1;
            arr2[i] = rd.nextInt(RANDOM_UPPER_BOUND) + 1;
        }

        // obliczanie wartosci
        int suma = 0;
        for (int i = 0; i < ARRAY_SIZE; i++) {
            suma += arr[i] * arr2[i];
        }
        System.out.println("Iloczyn skalarny wektorow to: " + suma);
    }
}
