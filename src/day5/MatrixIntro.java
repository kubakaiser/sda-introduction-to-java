package day5;

import java.util.Scanner;

public class MatrixIntro {

    public static int[][] createMatrix() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilosc wierszy: ");
        int mx = scanner.nextInt();
        System.out.println("Podaj ilosc kolumn: ");
        int my = scanner.nextInt();

        return new int[mx][my];
    }

    public static void fillMatrix(int[][] m) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                m[i][j] = scanner.nextInt();
            }
        }
    }

    public static void printMatrix(int[][] matrix) {
        // wyswietlenie zawartosci:
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " | ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static int[][] addMatrix(int[][] a, int[][] b) {
        // obsluzyc nie pasujace macierze
        // pytanie co powinno byc zwracane
        // jesli nie pasuja wymiary?
        if (!validate(a, b)) {
            return null;
        }

        int[][] result = new int[a.length][a[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = a[i][j] + b[i][j];
            }
        }
        return result;
    }

    public static boolean validate(int[][] a, int[][] b) {
        // Jezeli zewnetrzne wymiary sa rozne to od razu wiemy, ze macierze sie nie zgadzaja
        if (a.length != b.length) {
            return false;
        }
        // jezeli dotarlismy tutaj
        // to zewnetrzne wymiary sa ok
        //sprawdzamy wiersze
        for (int i = 0; i < a.length; i++) {
            // jezeli znajdziemy wiersz, gdzie ich liczba kolumn jest rozne
            // to zwroc falsz
            if (a[i].length != b[i].length) {
                return false;
            }
        }
        // jezeli doszliszmy az tutaj, to znaczy, ze wszystkie kolumny sa takie same
        return true;
    }

    public static void main(String[] args) {

        int[][] matrix = createMatrix();
        // podobnie dla drugiej macierzy
        int[][] secondMatrix = createMatrix();
        //wypelnienie wartosciami od uzytkownika
        fillMatrix(matrix);
        fillMatrix(secondMatrix);
        // drukowanie
        printMatrix(matrix);
        printMatrix(secondMatrix);
        //
        int[][] result = addMatrix(matrix, secondMatrix);
        if (result != null) {
            printMatrix(result);
        } else {
            System.out.println("Nie mozna dodac takich macierzy");
        }
    }
}
