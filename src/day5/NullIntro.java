package day5;

public class NullIntro {
    public static void main(String[] args) {
        String imie = "Kuba";
        System.out.println(imie.toUpperCase());

        imie = null;

        System.out.println(imie.toUpperCase());
    }
}
