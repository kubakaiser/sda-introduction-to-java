package day5;

import java.util.Scanner;

public class SearchStringInArray {
    public static void main(String[] args) {

        // Utworzyc tablice 5 napisow
        // i od razu ja zainicjalizowac
        // I wersja
        // Jezeli podamy zly indeks (za duzy, za maly)
        // to bedzie rzucony wyjatek ArrayIndexOutOfBoundsException
        String[] nazwiska = new String[5];
        nazwiska[0] = "Kowalski";
        nazwiska[1] = "Nowak";
//        nazwiska[2] = "Adamiak";
//        nazwiska[3] = "Nowacki";
        // Blad! Zbyt duzy indeks
//        nazwiska[9] = "Kowal";

        // Utworzenie tablicy 5 elementowej z inicjalizacja
        // Rozmiar tablicy jest wywnioskowany po liczbie elementow
        // podanych w nawiasach
        Scanner sc = new Scanner(System.in);
        String[] surnames = {"Kowalski", "Nowak", "Adamiak", "DotProductCalculator", "Nazwisko"};

        //Zapytac uzytkownika o nazwisko
        System.out.println("Podaj nazwisko: ");
        String nazwisko = sc.nextLine();
        boolean isPresent = false;
        for(int i = 0; i < surnames.length; i++){
            if(surnames[i].equals(nazwisko) ){
                isPresent = true;
                break;
            }
        }

        System.out.println(isPresent);

        //Odpowiedziec czy takie nazwisko wystepuje
    }
}
